﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Advent_of_Code_2020
{
  static class Day6
  {
		private static List<string> formatInput(string[] input)
		{
			List<string> passports = new List<string>();
			string passport = "";

			foreach (string line in input) {
				if (line != "")
					passport += line;
				else {
					passports.Add(passport);
					passport = "";
				}
			}

			passports.Add(passport);
			return passports;
		}

		private static Boolean charInAllList(char c, List<string> groupChoices)
    {
			foreach (string choice in groupChoices) {
				if (!choice.Contains(c))
					return false;
      }

			return true;
    }

		private static int checkAllCharsInList(List<string> list)
		{
			char[] chars = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
				'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
			int count = 0;

			foreach (char c in chars) {
				if (charInAllList(c, list))
					count++;
      }

			return count;
		}

		public static int partOne(string[] input)
    {
			List<string> groupChoices = formatInput(input);
			int count = 0;

			foreach (string s in groupChoices) {
				List<char> choices = new List<char>();
				foreach (char c in s) {
					choices.Add(c);
        }

				count += new HashSet<char>(choices).Count;
      }

			return count;
    }

		public static int partTwo(string[] input)
    {
			int count = 0;
			List<string> currentGroup = new List<string>();
			
			foreach (string line in input) {
				if (line != "") {
					currentGroup.Add(line);
				} else {
					count += checkAllCharsInList(currentGroup);
					currentGroup = new List<string>();
				}
      }

			count += checkAllCharsInList(currentGroup);
			return count;
    }
	}
}

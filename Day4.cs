﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Advent_of_Code_2020
{
	static class Day4
	{
		private static List<string> formatInput(string[] input)
		{
			List<string> passports = new List<string>();
			string passport = "";

			foreach (string line in input) {
				if (line != "")
					passport += (line + " ");
				else {
					passports.Add(passport);
					passport = "";
				}
			}

			passports.Add(passport);
			return passports;
		}

		private static List<IDictionary<string, string>> getPassports(List<string> input)
		{
			List<IDictionary<string, string>> passports = new List<IDictionary<string, string>>();

			foreach (string passport in input) {
				IDictionary<string, string> newDic = new Dictionary<string, string>();

				string[] fields = passport.Split(" ");

				foreach (string field in fields) {
					string[] data = field.Split(":");
					if (data.Length == 2)
						newDic.Add(data[0], data[1]);
				}
				passports.Add(newDic);
			}

			return passports;
		}

		private static Boolean isValidPassport(IDictionary<string, string> passport)
		{
			if (!((passport.ContainsKey("byr")) &&
					(passport.ContainsKey("iyr")) &&
					(passport.ContainsKey("eyr")) &&
					(passport.ContainsKey("hgt")) &&
					(passport.ContainsKey("hcl")) &&
					(passport.ContainsKey("ecl")) &&
					(passport.ContainsKey("pid")))) {
				return false;
			}
				
			if (!((Int32.Parse(passport["byr"]) >= 1920) && (Int32.Parse(passport["byr"]) <= 2002)))
				return false;

			if (!((Int32.Parse(passport["iyr"]) >= 2010) && (Int32.Parse(passport["iyr"]) <= 2020)))
				return false;

			if (!((Int32.Parse(passport["eyr"]) >= 2020) && (Int32.Parse(passport["eyr"]) <= 2030)))
				return false;

			if (passport["hgt"][passport["hgt"].Length - 1] == 'm') {
				if (!Char.IsDigit(passport["hgt"][2]) ||
						!((Int32.Parse(passport["hgt"].Substring(0, 3)) >= 150) && (Int32.Parse(passport["hgt"].Substring(0, 3)) <= 193)))
					return false;
				} else {
				if (!Char.IsDigit(passport["hgt"][1]) ||
						!((Int32.Parse(passport["hgt"].Substring(0, 2)) >= 59) && (Int32.Parse(passport["hgt"].Substring(0, 2)) <= 76)))
					return false;
			}

			if (!(passport["hcl"][0] == '#'))
				return false;

			if (passport["hcl"].Length != 7)
				return false;

			foreach (char c in passport["hcl"]) {
				if ((!Char.IsNumber(c)) &&
						(c != '#') &&
						((c != 'a') && (c != 'b') && (c != 'c') && (c != 'd') && (c != 'e') && (c != 'f')))
				return false;
			}

			if ((passport["ecl"] != "amb") && (passport["ecl"] != "blu") && (passport["ecl"] != "brn") &&
					(passport["ecl"] != "gry") && (passport["ecl"] != "grn") && (passport["ecl"] != "hzl") && (passport["ecl"] != "oth"))
				return false;

			if (passport["pid"].Length != 9)
				return false;

			foreach(char c in passport["pid"]) {
				if (!Char.IsNumber(c))
					return false;
			}

			return true;
		}

		public static int partOne(string[] input)
		{
			List<IDictionary<string, string>> passports = getPassports(formatInput(input));
			int valid = 0;

			foreach(IDictionary<string, string> passport in passports) {
				if ((passport.ContainsKey("byr")) &&
						(passport.ContainsKey("iyr")) &&
						(passport.ContainsKey("eyr")) &&
						(passport.ContainsKey("hgt")) &&
						(passport.ContainsKey("hcl")) &&
						(passport.ContainsKey("ecl")) &&
						(passport.ContainsKey("pid")))
					valid++;
			}

			return valid;
		}

		public static int partTwo(string[] input)
		{
			List<IDictionary<string, string>> passports = getPassports(formatInput(input));
			int valid = 0;

			foreach(IDictionary<string, string> passport in passports) {
				if (isValidPassport(passport))
					valid++;
			}

			return valid;
		}
	}
}
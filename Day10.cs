﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Advent_of_Code_2020
{
  static class Day10
  {
    private static int[] processInput(string[] input)
    {
      List<int> list = new List<int>();
      list.Add(0);

      foreach (string s in input)
        list.Add(Int32.Parse(s.Trim()));

      list.Sort();
      
      int[] sortedList = list.ToArray();

      return sortedList;
    }

    public static int partOne(string[] input)
    {
      int[] adapters = processInput(input);
      int oneJolt = 0;
      int threeJolt = 0;
      int prevJolt = 0;
      int currJolt;

      for (int i = 1; i < adapters.Length; i++) {
        currJolt = adapters[i];

        if ((currJolt - prevJolt) == 1)
          oneJolt++;
        if ((currJolt - prevJolt) == 3)
          threeJolt++;

        prevJolt = currJolt;
      }

      threeJolt++;

      return (oneJolt * threeJolt);
    }

    public static long partTwo(string[] input)
    {
      int[] adapters = processInput(input);
      long[] paths = new long[adapters.Length];
      Array.Clear(paths, 0, paths.Length);

      paths[0] = 1;

      for (int i = 0; i < adapters.Length; i++) {
        if ((i + 1) < adapters.Length) {
          if ((adapters[i + 1] - adapters[i]) <= 3)
            paths[i + 1] += paths[i];
        }
        if ((i + 2) < adapters.Length) {
          if ((adapters[i + 2] - adapters[i]) <= 3)
            paths[i + 2] += paths[i];
        }
        if ((i + 3) < adapters.Length) {
          if ((adapters[i + 3] - adapters[i]) <= 3)
            paths[i + 3] += paths[i];
        }
      }

      return paths[paths.Length - 1];
    }
  }
}

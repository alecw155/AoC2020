﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advent_of_Code_2020
{
  static class Day14
  {
    private static string ReplaceFirst(string text, string search, string replace)
    {
      int pos = text.IndexOf(search);
      if (pos < 0) {
        return text;
      }
      return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
    }

    private static string getMask(string s)
    {
      string[] buffer = s.Split("mask = ", StringSplitOptions.RemoveEmptyEntries);
      return buffer[0];
    }

    private static long[] getMem(string s)
    {
      long[] dict = new long[2];
      string[] separators = new string[] { "mem[", "] = " };
      string[] buffer;

      buffer = s.Split(separators, StringSplitOptions.RemoveEmptyEntries);
      dict[0] = Int64.Parse(buffer[0].Trim());
      dict[1] = Int64.Parse(buffer[1].Trim());

      return dict;
    }

    private static long bitToInt(string s)
    {
      long total = 0;

      for (int i = 0; i < s.Length; i++) {
        if (s[s.Length - 1 - i] == '1') {
          total += (long)Math.Pow(2, i);
        }
      }

      return total;
    }

    private static string applyMask(string bits, string mask, Boolean excludeX)
    {
      char[] newBits = bits.ToCharArray();

      for (int i = 0; i < mask.Length; i++) {
        if (mask[i] != (excludeX ? 'X' : '0'))
          newBits[i] = mask[i];
      }

      return new string(newBits);
    }

    private static long sumDict(Dictionary<long, long> dict)
    {
      long total = 0;

      foreach (long val in dict.Values) {
        total += val;
      }

      return total;
    }

    private static List<string> getAddresses(string s)
    {
      List<string> addresses = new List<string>();

      for (int i = 0; i < s.Length; i++) {
        if (s[i] == 'X') {
          addresses = getAddresses(ReplaceFirst(s, "X", "0"))
                      .Concat(getAddresses(ReplaceFirst(s, "X", "1")))
                      .ToList();

          return addresses;
        }
      }

      addresses.Add(s);
      return addresses;
    }

    public static long partOne(string[] input)
    {
      Dictionary<long, long> mem = new Dictionary<long, long>();
      string mask = "";
      long[] buffer;

      foreach (string s in input) {
        if (s[1] == 'a') {
          mask = getMask(s);
        } else {
          buffer = getMem(s);

          string bits = Convert.ToString(buffer[1], 2).PadLeft(36, '0');
          buffer[1] = bitToInt(applyMask(bits, mask, true));

          if (mem.ContainsKey(buffer[0])) {
            mem[buffer[0]] = buffer[1];
          } else {
            mem.Add(buffer[0], buffer[1]);
          }
        }
      }

      return sumDict(mem);
    }

    public static long partTwo(string[] input)
    {
      Dictionary<long, long> mem = new Dictionary<long, long>();
      string mask = "";
      long[] buffer;

      foreach (string s in input) {
        if (s[1] == 'a') {
          mask = getMask(s);
        } else {
          buffer = getMem(s);

          string bits = Convert.ToString(buffer[0], 2).PadLeft(36, '0');
          List<string> newAddresses = getAddresses(applyMask(bits, mask, false));

          foreach (string address in newAddresses) {
            long addressNum = bitToInt(address);
            if (mem.ContainsKey(addressNum)) {
              mem[addressNum] = buffer[1];
            } else {
              mem.Add(addressNum, buffer[1]);
            }
          }
          
        }
      }

      return sumDict(mem);
    }
  }
}

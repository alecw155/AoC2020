﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Advent_of_Code_2020
{
  class Bag
  {
    public string desc { get; }
    public Dictionary<string, int> contents { get; }

    public Bag(string _desc, Dictionary<string, int> _contents)
    {
      desc = _desc;
      contents = _contents;
    }
  }

  static class Day7
  {
    private static List<Bag> processInput(string[] input)
    {
      List<Bag> bags = new List<Bag>();
      string[] separators = new string[] { "bags", "bag", " contain ", ", ", "." };

      foreach (string s in input) {
        string[] substrings = s.Split(separators, StringSplitOptions.RemoveEmptyEntries);
        Dictionary<string, int> contents = new Dictionary<string, int>();

        for (int i = 1; i < substrings.Length; i++) {
          if (!substrings[i].Contains("no other"))
            contents.Add(substrings[i].Substring(2).Trim(), Int32.Parse(substrings[i][0].ToString()));
        }

        bags.Add(new Bag(substrings[0].Trim(), contents));
      }

      return bags;
    }

    private static Boolean canContainGold(Bag bag, List<Bag> allBags)
    {
      foreach (string childBag in bag.contents.Keys) {
        if ((childBag == "shiny gold") || (canContainGold(allBags.Find(b => b.desc == childBag), allBags)))
          return true;
      }

      return false;
    }

    private static int sumContents(Bag bag, List<Bag> allBags)
    {
      int count = 0;

      if (bag.contents.Count == 0)
        return 0;

      foreach(string childBag in bag.contents.Keys) {
        count += (bag.contents[childBag] + (bag.contents[childBag] * sumContents(allBags.Find(x => x.desc == childBag), allBags)));
      }

      return count;
    }

    public static int partOne(string[] input)
    {
      int count = 0; 
      List<Bag> allBags = processInput(input);

      foreach (Bag bag in allBags) {
        if (canContainGold(bag, allBags))
          count++;
      }

      return count;
    }

    public static int partTwo(string[] input)
    {
      List<Bag> allBags = processInput(input);

      return sumContents(allBags.Find(x => x.desc == "shiny gold"), allBags);
    }
  }
}

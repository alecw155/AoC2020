﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Advent_of_Code_2020
{
  enum Directions
  {
    East,
    South,
    West,
    North,
  }

  class Day12
  {
    private static int mod(int x, int m)
    {
      return (x % m + m) % m;
    }

    private static Directions rotate(Directions currHeading, char dir, int degrees)
    {
      Directions[] headings = new Directions[] { Directions.East, Directions.South, Directions.West, Directions.North };
      int index = Array.IndexOf(headings, currHeading);

      index = mod((degrees / 90) * (dir == 'R' ? 1 : -1) + index, 4);
      return headings[index];
    }

    private static int[] rotateWaypoint(char dir, int degrees, int xPos, int yPos)
    {
      int[] newPos = new int[] { xPos, yPos };

      degrees = (degrees / 90);

      while (degrees != 0) {
        if (dir == 'R') {
          int temp = newPos[0];
          newPos[0] = newPos[1];
          newPos[1] = temp;
          newPos[1] *= -1;
        } else if (dir == 'L') {
          int temp = newPos[0];
          newPos[0] = newPos[1];
          newPos[1] = temp;
          newPos[0] *= -1;
        }

        degrees--;
      }

      return newPos;
    }

    public static int partOne(string[] input)
    {
      int xPos = 0;
      int yPos = 0;
      Directions currHeading = Directions.East;

      foreach (string s in input) {
        int val = Int32.Parse(s.Substring(1).Trim());

        switch (s[0]) {
          case ('E'):
            xPos += val;
            break;
          case ('S'):
            yPos -= val;
            break;
          case ('W'):
            xPos -= val;
            break;
          case ('N'):
            yPos += val;
            break;
          case ('F'):
            switch (currHeading) {
              case (Directions.East):
                xPos += val;
                break;
              case (Directions.South):
                yPos -= val;
                break;
              case (Directions.West):
                xPos -= val;
                break;
              case (Directions.North):
                yPos += val;
                break;
            }
            break;
          default:
            currHeading = rotate(currHeading, s[0], val);
            break;
        }
      }

      return (Math.Abs(xPos) + Math.Abs(yPos));
    }

    public static int partTwo(string[] input)
    {
      int shipXPos = 0;
      int shipYPos = 0;
      int waypointXpos = 10;
      int waypointYpos = 1;

      foreach (string s in input) {
        int val = Int32.Parse(s.Substring(1).Trim());

        switch (s[0]) {
          case ('E'):
            waypointXpos += val;
            break;
          case ('S'):
            waypointYpos -= val;
            break;
          case ('W'):
            waypointXpos -= val;
            break;
          case ('N'):
            waypointYpos += val;
            break;
          case ('F'):
            shipXPos += waypointXpos * val;
            shipYPos += waypointYpos * val;
            break;
          default:
            int[] newWaypoint = rotateWaypoint(s[0], val, waypointXpos, waypointYpos);
            waypointXpos = newWaypoint[0];
            waypointYpos = newWaypoint[1];
            break;
        }
      }

      return (Math.Abs(shipXPos) + Math.Abs(shipYPos));
    }
  }
}

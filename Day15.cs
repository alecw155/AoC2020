﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advent_of_Code_2020
{
  static class Day15
  {
    public static int partOne(string[] input)
    {
      List<string> list = new List<string>(input[0].Split(',', StringSplitOptions.RemoveEmptyEntries));

      while (list.Count < 2020) {
        string value = list[list.Count - 1];
        int count = list.Where(x => x == value).Count();

        if (count == 1) {
          list.Add("0");
        } else {
          int last = list.LastIndexOf(value);
          List<string> copy = new List<string>(list);
          copy.RemoveAt(last);
          int secondLast = copy.LastIndexOf(value);
          list.Add((last - secondLast).ToString());
        }
      }

      return Int32.Parse(list[list.Count-1]);
    }

    public static int partTwo(string[] input)
    {
      int[][] list = new int[30000000][];
      List<string> initialList = new List<string>(input[0].Split(',', StringSplitOptions.RemoveEmptyEntries));
      int i = 1;
      int lastVal = -1;

      
      for (; i - 1 < initialList.Count(); i++) {
        list[Int32.Parse(initialList[i - 1])] = new int[] { i, -1 };
        lastVal = Int32.Parse(initialList[i - 1]);
      }

      while (i <= 30000000) {
        if (list[lastVal] != null) {
          if (list[lastVal][1] == -1) {
            if (list[0] != null) {
              list[0] = new int[] { i, list[0][0] };
            } else {
              list[0] = new int[] { i, -1 };
            }

            lastVal = 0;
          } else {
            int buffer = list[lastVal][0] - list[lastVal][1];

            if (list[buffer] != null) {
              list[buffer] = new int[] { i, list[buffer][0] };
            } else {
              list[buffer] = new int[] { i, -1 };
            }

            lastVal = buffer;
          }
        }
        i++; 
        Console.WriteLine("Remaining: " + (30000000 - i));
      }

      return lastVal;
    }
  }
}

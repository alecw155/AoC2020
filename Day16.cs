﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Advent_of_Code_2020
{
  static class Day16
  {
    private static int[] getValues(string input)
    {
      string[] stringVals = input.Split(',', StringSplitOptions.RemoveEmptyEntries);
      int[] values = new int[stringVals.Length];

      for (int i = 0; i < stringVals.Length; i++) {
        values[i] = Int32.Parse(stringVals[i]);
      }

      return values;
    }

    private static Boolean inRange(int value, Dictionary<string, int[]> ranges)
    {
      foreach (int[] range in ranges.Values) {
        if (((range[0] <= value) && (value <= range[1])) || ((range[2] <= value) && (value <= range[3]))) {
          return true; 
        }
      }

      return false;
    }

    private static List<string> getFittingRanges(int value, Dictionary<string, int[]> rules)
    {
      List<string> fittingRanges = new List<string>();

      foreach (var rule in rules) {
        if (((rule.Value[0] <= value) && (value <= rule.Value[1])) || ((rule.Value[2] <= value) && (value <= rule.Value[3]))) {
          fittingRanges.Add(rule.Key);
        }
      }

      return fittingRanges;
    }

    private static Dictionary<string, int[]> getRules(string[] input)
    {
      string pattern = @"(.+): (\d+)-(\d+) or (\d+)-(\d+)";
      Regex rx = new Regex(pattern, RegexOptions.Compiled);
      Dictionary<string, int[]> rules = new Dictionary<string, int[]>();

      for (int i = 0; (i < input.Length) && (input[i] != ""); i++) {
        var match = rx.Match(input[i]);
        rules.Add(match.Groups[1].Value, new int[] { Int32.Parse(match.Groups[2].Value),
                                                           Int32.Parse(match.Groups[3].Value),
                                                           Int32.Parse(match.Groups[4].Value),
                                                           Int32.Parse(match.Groups[5].Value)});
      }

      return rules;
    }

    private static List<int[]> getValidTickets(int i, string[] input, Dictionary<string, int[]> rules)
    {
      Boolean add = true;
      List<int[]> validTickets = new List<int[]>();

      for (; i < input.Length; i++) {
        int[] ticket = getValues(input[i]);

        foreach (int field in ticket) {
          if (!inRange(field, rules)) {
            add = false;
            break;
          }
        }

        if (add)
          validTickets.Add(ticket);

        add = true;
      }

      return validTickets;
    }

    public static int partOne(string[] input)
    {
      int total = 0;
      int i = 0;
      Dictionary<string, int[]> rules;

      rules = getRules(input);

      for (; (i < input.Length) && (input[i] != ""); i++) { }
      i += 5;

      for (; i < input.Length; i++) {
        int[] values = getValues(input[i]);

        foreach (int value in values) {
          if (!inRange(value, rules)) {
            total += value;
          }
        }
      }

      return total;
    }

    public static long partTwo(string[] input)
    {
      int i = 0;
      long total = 1;
      int[] myTicket;
      List<int[]> validTickets;
      Dictionary<string, int[]> rules;
      Dictionary<int, List<string>> rangesOverColumn = new Dictionary<int, List<string>>();
      Dictionary<string, int> finalRanges = new Dictionary<string, int>();

      rules = getRules(input);

      for (; (i < input.Length) && (input[i] != ""); i++) {}
      i += 2;
      
      myTicket = getValues(input[i]);

      i += 3;

      validTickets = getValidTickets(i, input, rules);

      for (i = 0; i < validTickets[0].Length; i++) {
        List<string> columnRanges = new List<string>();

        for (int j = 0; j < validTickets.Count; j++) { 
          if (columnRanges.Count == 0) {
            columnRanges = getFittingRanges(validTickets[j][i], rules);
          } else {
            columnRanges = columnRanges.Intersect(getFittingRanges(validTickets[j][i], rules)).ToList();
          }
        }

        rangesOverColumn.Add(i, columnRanges);
      }

      while (rangesOverColumn.Count != 0) {
        foreach (var currentColumn in rangesOverColumn) {
          if (currentColumn.Value.Count == 1) {
            finalRanges.Add(currentColumn.Value.First(), currentColumn.Key);

            foreach (var otherColumn in rangesOverColumn) {
              if (otherColumn.Key != currentColumn.Key)
                otherColumn.Value.Remove(currentColumn.Value.First());
            }

            rangesOverColumn.Remove(currentColumn.Key);
          }
        }
      }

      foreach (var s in finalRanges) {
        if (s.Key.Contains("departure")) {
          total *= myTicket[s.Value];
        }
      }

      return total;
    }
  }
}

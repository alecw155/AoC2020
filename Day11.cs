﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advent_of_Code_2020
{
  static class Day11
  {
    public static string ReplaceAt(this string input, int index, char newChar)
    {
      if (input == null) {
        throw new ArgumentNullException("input");
      }
      char[] chars = input.ToCharArray();
      chars[index] = newChar;
      return new string(chars);
    }

    private static string[] getAdjacent(string[] seats, int row, int col)
    {
      string[] adjacent = new string[3];

      if (col == 0) {

        for (int i = 0; i < 3; i++)
          if (((row - 1 + i) >= 0) && ((row - 1 + i) < seats.Length))
            adjacent[i] = seats[row - 1 + i].Substring(0, 2);

      } else if (col == seats[0].Length - 1) {

        for (int i = 0; i < 3; i++)
          if (((row - 1 + i) >= 0) && ((row - 1 + i) < seats.Length))
            adjacent[i] = seats[row - 1 + i].Substring(seats[row].Length - 2, 2);

      } else {
   
        for (int i = 0; i < 3; i++)
          if (((row - 1 + i) >= 0) && ((row - 1 + i) < seats.Length))
            adjacent[i] = seats[row - 1 + i].Substring(col - 1, 3);
      }

      adjacent = adjacent.Where(c => c != null).ToArray();

      return adjacent;
    }

    private static string[] getLoSAdjacent(string[] seats, int row, int col)
    {
      string[] adjacent = new string[3];
      for (int i = 0; i < adjacent.Length; i++)
        adjacent[i] = "...";

      adjacent[1] = adjacent[1].ReplaceAt(1, seats[row][col]);

      for (int i = 1; i < seats.Length; i++) {
        //top left
        if (((row - i) >= 0) && ((col - i) >= 0))
          if ((seats[row - i][col - i] != '.') && (adjacent[0][0] == '.'))
            adjacent[0] = adjacent[0].ReplaceAt(0, seats[row - i][col - i]);
        //top 
        if ((row - i) >= 0)
          if ((seats[row - i][col] != '.') && (adjacent[0][1] == '.'))
            adjacent[0] = adjacent[0].ReplaceAt(1, seats[row - i][col]);
        //top right
        if (((row - i) >= 0) && ((col + i) < seats[row].Length))
          if ((seats[row - i][col + i] != '.') && (adjacent[0][2] == '.'))
            adjacent[0] = adjacent[0].ReplaceAt(2, seats[row - i][col + i]);

        //left
        if ((col - i) >= 0)
          if ((seats[row][col - i] != '.') && (adjacent[1][0] == '.'))
            adjacent[1] = adjacent[1].ReplaceAt(0, seats[row][col - i]);
        //right
        if ((col + i) < seats[row].Length)
          if ((seats[row][col + i] != '.') && (adjacent[1][2] == '.'))
            adjacent[1] = adjacent[1].ReplaceAt(2, seats[row][col + i]);

        //bottom left
        if (((row + i) < seats.Length) && ((col - i) >= 0))
          if ((seats[row + i][col - i] != '.') && (adjacent[2][0] == '.'))
            adjacent[2] = adjacent[2].ReplaceAt(0, seats[row + i][col - i]);
        //bottom
        if ((row + i) < seats.Length)
          if ((seats[row + i][col] != '.') && (adjacent[2][1] == '.'))
            adjacent[2] = adjacent[2].ReplaceAt(1, seats[row + i][col]);
        //bottom right
        if (((row + i) < seats.Length) && ((col + i) < seats[0].Length))
          if ((seats[row + i][col + i] != '.') && (adjacent[2][2] == '.'))
            adjacent[2] = adjacent[2].ReplaceAt(2, seats[row + i][col + i]);
      }

      return adjacent;
    }

    private static Boolean shouldBecomeOccupied(string[] seats, char middle)
    {
      foreach (string s in seats)
        if (s.Contains('#'))
          return false;

      return (middle != '.');
    }

    private static Boolean shouldBecomeEmpty(string[] seats, char middle)
    {
      int total = -1;

      foreach (string s in seats)
        total += s.Count(c => c == '#');

      return ((total >= 4) && (middle != '.') && (middle != 'L'));
    }

    private static Boolean shouldBecomeEmptyAlt(string[] seats, char middle)
    {
      int total = -1;

      foreach (string s in seats)
        total += s.Count(c => c == '#');

      return ((total >= 5) && (middle != '.') && (middle != 'L'));
    }

    private static void emptySeats(List<int[]> flip, string[] seats)
    {
      foreach (int[] index in flip) {
        if (seats[index[0]][index[1]] == '#')
          seats[index[0]] = seats[index[0]].ReplaceAt(index[1], 'L');
      }
    }

    private static void occupySeats(List<int[]> flip, string[] seats)
    {
      foreach (int[] index in flip) {
        if (seats[index[0]][index[1]] == 'L')
          seats[index[0]] = seats[index[0]].ReplaceAt(index[1], '#');
      }
    }

    public static int partOne(string[] input)
    {
      string[] inputCopy = new string[input.Length];
      input.CopyTo(inputCopy, 0);
      List<int[]> toEmpty = new List<int[]>();
      List<int[]> toOccupy = new List<int[]>();
      int total = 0;
      
      do {
        toEmpty.Clear();
        toOccupy.Clear();

        for (int i = 0; i < inputCopy.Length; i++) {
          for (int j = 0; j < inputCopy[i].Length; j++) {
            char seat = inputCopy[i][j];
            string[] adjacent = getAdjacent(inputCopy, i, j);

            if (shouldBecomeEmpty(adjacent, seat)) {
              toEmpty.Add(new int[] { i, j });
            }
              
            if (shouldBecomeOccupied(adjacent, seat))
              toOccupy.Add(new int[] { i, j });
          }
        }

        emptySeats(toEmpty, inputCopy);
        occupySeats(toOccupy, inputCopy);

      } while ((toEmpty.Count + toOccupy.Count) != 0);

      foreach (string s in inputCopy)
        total += s.Count(c => c == '#');

      return total;
    }

    public static int partTwo(string[] input)
    {
      string[] inputCopy = new string[input.Length];
      input.CopyTo(inputCopy, 0);
      List<int[]> toEmpty = new List<int[]>();
      List<int[]> toOccupy = new List<int[]>();
      int total = 0;

      do {
        toEmpty.Clear();
        toOccupy.Clear();

        for (int i = 0; i < inputCopy.Length; i++) {
          for (int j = 0; j < inputCopy[i].Length; j++) {
            char seat = inputCopy[i][j];
            string[] adjacent = getLoSAdjacent(inputCopy, i, j);

            if (shouldBecomeEmptyAlt(adjacent, seat))
              toEmpty.Add(new int[] { i, j });

            if (shouldBecomeOccupied(adjacent, seat))
              toOccupy.Add(new int[] { i, j });
          }
        }

        emptySeats(toEmpty, inputCopy);
        occupySeats(toOccupy, inputCopy);

      } while ((toEmpty.Count + toOccupy.Count) != 0);

      foreach (string s in inputCopy)
        total += s.Count(c => c == '#');

      return total;
    }
  }
}

﻿using System;
using System.IO;

namespace Advent_of_Code_2020
{
	class Program
	{
		static int Main(string[] args)
		{
			if (args.Length <= 0) 
				return -1;

			var input = File.ReadAllLines(@args[0]);

			//Console.WriteLine(Day14.partOne(input));
			//Console.WriteLine(Day14.partTwo(input));

			//Console.WriteLine(Day15.partOne(input));
			//Console.WriteLine(Day15.partTwo(input));

			Console.WriteLine(Day16.partOne(input));
			Console.WriteLine(Day16.partTwo(input));

			return 0;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Advent_of_Code_2020
{
  static class Day5
  {
    private static int getRow(string input)
    {
      int min = 0;
      int max = 127;
      int d;

      foreach (char c in input.Substring(0, 6)) {
        d = ((max - min) + 1) / 2;
        if (c == 'F') 
          max = (max - d);
        else 
          min = (min + d);
      }

      return (input[6] == 'F' ? min : max);
    }

    private static int getColumn(string input)
    {
      int min = 0;
      int max = 7;
      int d;

      foreach (char c in input.Substring(7, 3)) {
        d = ((max - min) + 1) / 2;
        if (c == 'L')
          max = (max - d);
        else
          min = (min + d);
      }

      return (input[6] == 'L' ? min : max);
    }

    public static int partOne(string[] input)
    {
      int current;
      int max = 0;

      foreach (string s in input) {
        current = (getRow(s) * 8) + getColumn(s);
        if (current > max)
          max = current;
      }

      return max;
    }

    public static int partTwo(string[] input)
    {
      int prev;
      int current = 0;
      List<int> list = new List<int>();

      foreach (string s in input) 
        list.Add((getRow(s) * 8) + getColumn(s));

      list.Sort();

      foreach (int n in list) {
        prev = current;
        current = n;
        if (current - prev == 2)
          return (prev + 1);
      }

      return -1;
    }
  }
}
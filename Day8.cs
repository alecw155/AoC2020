﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Advent_of_Code_2020
{
  static class Day8
  {
    private static int getValue(string value)
    {
      int sign = (value[0] == '+' ? 1 : -1);
      return (Int32.Parse(value.Substring(1).Trim()) * sign);
    }

    private static int tryProgram(string[] input)
    {
      int index = 0;
      List<int> pastIndexes = new List<int>();
      int accumulator = 0;
      string[] op;
      int value;

      pastIndexes.Add(index);

      while ((index >= 0) && (index < input.Length)) {
        op = input[index].Split(" ", StringSplitOptions.RemoveEmptyEntries);
        value = getValue(op[1]);

        switch (op[0]) {
          case ("nop"):
            index++;
            break;
          case ("acc"):
            accumulator += value;
            index++;
            break;
          case ("jmp"):
            index += value;
            break;
          default:
            break;
        }

        if (pastIndexes.Contains(index))
          return -1;

        pastIndexes.Add(index);
      }

      return accumulator;
    }

    public static int partOne(string[] input)
    {
      int index = 0;
      List<int> pastIndexes = new List<int>();
      int accumulator = 0;
      string[] op;
      int value;

      pastIndexes.Add(index);

      while (index >= 0) {
        op = input[index].Split(" ", StringSplitOptions.RemoveEmptyEntries);
        value = getValue(op[1]);

        switch (op[0]) {
          case ("nop"):
            index++;
            break;
          case ("acc"):
            accumulator += value;
            index++;
            break;
          case ("jmp"):
            index += value;
            break;
          default:
            break;
        }

        if (pastIndexes.Contains(index))
          return accumulator;

        pastIndexes.Add(index);
      }

      return accumulator;
    }

    public static int partTwo(string[] input)
    {
      int value = 0;
      string[] inputCopy = new string[input.Length];
      input.CopyTo(inputCopy, 0);

      for (int i = 0; i < input.Length; i++) {
        if (input[i][0] == 'j') {
          inputCopy[i] = inputCopy[i].Replace("jmp", "nop");

          value = tryProgram(inputCopy);
          if (value != -1)
            return value;

          input.CopyTo(inputCopy, 0);
        } else if (input[i][0] == 'n') {
          inputCopy[i] = inputCopy[i].Replace("nop", "jmp");

          value = tryProgram(inputCopy);
          if (value != -1)
            return value;

          input.CopyTo(inputCopy, 0);
        }
      }

      return value;
    }
  }
}
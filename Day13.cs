﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Advent_of_Code_2020
{
  static class Day13
  {
    private static long getLCM(long a, long b)
    {
      long num1, num2;
      if (a > b) {
        num1 = a; num2 = b;
      } else {
        num1 = b; num2 = a;
      }

      for (int i = 1; i < num2; i++) {
        long mult = num1 * i;
        if (mult % num2 == 0) {
          return mult;
        }
      }
      return num1 * num2;
    }

    private static List<int> processInput(string input)
    {
      List<int> buses = new List<int>();
      string[] splitInput = input.Split(",");

      foreach (string s in splitInput) {
        if (s != "x")
          buses.Add(Int32.Parse(s));
      }

      return buses;
    }

    private static int[] processInputPartTwo(string input)
    {
      string[] splitInput = input.Split(",");
      int[] buses = new int[splitInput.Length];

      for (int i = 0; i < splitInput.Length; i++) {
        if (splitInput[i] != "x")
          buses[i] = Int32.Parse(splitInput[i]);
        else
          buses[i] = -1;
      }

      return buses;
    }

    private static Dictionary<long, long> getOffsets(int[] buses)
    {
      Dictionary<long, long> offsets = new Dictionary<long, long>();

      for (int i = 0; i < buses.Length; i++) {
        if (buses[i] != -1) {
          offsets.Add(buses[i], i);
        }
      }

      return offsets;
    }

    public static long partOne(string[] input)
    {
      int timestamp = Int32.Parse(input[0]);
      List<int> buses = processInput(input[1]);
      int waitTime = Int32.MaxValue;
      int quickestBus = Int32.MaxValue;

      foreach (int bus in buses) {
        int busTimestamp = ((int)Math.Ceiling((decimal)timestamp / bus) * bus);

        if (((busTimestamp - timestamp) < waitTime) && (busTimestamp >= timestamp)) {
          waitTime = busTimestamp - timestamp;
          quickestBus = bus;
        }
      }

      return waitTime * quickestBus;
    }

    public static long partTwo(string[] input)
    {
      int[] buses = processInputPartTwo(input[1]);
      Dictionary<long, long> offsets = getOffsets(buses);
      long step;
      long t = 0;
      long prevBus = 1;
      long prevPrevBus = 1;

      foreach (var item in offsets) {

        while ((t + item.Value) % item.Key != 0) {
          step = getLCM(prevBus, prevPrevBus);
          t += step;
        }

        prevPrevBus *= prevBus;
        prevBus = item.Key;
      }

      return t;
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Advent_of_Code_2020
{
  static class Day9
  {
    public static T[] SubArray<T>(this T[] data, int index, int length)
    {
      T[] result = new T[length];
      Array.Copy(data, index, result, 0, length);
      return result;
    }

    private static long[] processInput(string[] input)
    {
      long[] newList = new long[input.Length];

      for (int i = 0; i < input.Length; i++) {
        newList[i] = Int64.Parse(input[i].Trim());
      }

      return newList;
    }

    private static Boolean isValidSum(long value, long[] list)
    {
      for (int i = 0; i + 1 < list.Length; i++) {
        for (int j = i + 1; j < list.Length; j++) {
          if (list[i] + list[j] == value)
            return true;
        }
      }

      return false;
    }

    private static Boolean isValueInRangeSum(long value, long[] list)
    {
      long sum = 0;

      foreach (long l in list)
        sum += l;

      Console.WriteLine("invalid: " + value + ", sum: " + sum + ", value == sum: " + (value == sum));
      return (sum == value);
    }

    private static long getMin(long[] list)
    {
      long min = Int64.MaxValue;

      foreach (long n in list) {
        if (n < min)
          min = n;
      }

      return min;
    }

    private static long getMax(long[] list)
    {
      long max = 0;

      foreach (long n in list) {
        if (n > max)
          max = n;
      }

      return max;
    }

    public static long partOne(string[] input)
    {
      int startIndex = 0;
      int preamble = 25;
      long[] list = processInput(input);

      for (int i = preamble; i < list.Length; i++) {
        if (!isValidSum(list[i], list.SubArray(startIndex + (i - preamble), preamble)))
          return list[i];
      }

      return -1;
    }

    public static long partTwo(string[] input)
    {
      long invalid = partOne(input);
      long[] list = processInput(input);

      for (int i = 0; i < list.Length - 1; i++) {
        for (int j = 2; j + i < list.Length; j++) {
          if (isValueInRangeSum(invalid, list.SubArray(i, j)))
            return getMax(list.SubArray(i, j)) + getMin(list.SubArray(i, j));
        }
      }

      return -1;
    }
  }
}
